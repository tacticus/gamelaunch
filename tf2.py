import argparse
import sys
import os
import time
import subprocess
__author__ = 'priorax'
 
parser = argparse.ArgumentParser(description='Demo of script')
parser.add_argument('-n','--host',help='Server hostname, for multi word hostnames, please put in quotes, eg: "Respawn V27 Server #',required=True)
parser.add_argument('-c','--count',help='Server count (number of servers to be run)',required=True)
parser.add_argument('-r','--rcon',help='RCON password (administration password)',required=True)
parser.add_argument('-l','--type',help='Type of server (comp or server)', required=True)
parser.add_argument('-s','--svpass',help='Set sv_password',required=False)
parser.add_argument('-t','--tvname',help='STV name', required=False)
args = parser.parse_args()

def passwordGen(serverName):
   password = ""
   i = 0
   while serverName[i] != " ":
      password += serverName[i].lower()
      i += 1
   return password

def main():
# wait for dhcp
   time.sleep(2)
   default_launch = "tf2/srcds_run -game tf +sv_lan 1 +rcon_password \"%s\"" % (args.rcon)
   default_launch +=" +sv_pure 2 +maxplayers 13 +tv_enable 1"
   servertype = args.type.lower()
   if servertype != "comp" and servertype != "casual":
      print "This is not a valid type, please try again"
      sys.exit(1)
   if args.svpass:
      password = args.svpass
   else:
      password = passwordGen(args.host)
    
   if servertype == "comp":
      for i in range(1, int(args.count) + 1):
         hostname = args.host + str(i)
         tvname = ""
         if(args.tvname):
            tvname = args.tvname + str(i)
         else:
            tvname = "%s STV" % (hostname)
            cfg = "%s.cfg" % (password + str(i))
	    cfg_file_loc = "tf2/tf/cfg/" + cfg
            if os.path.exists(cfg_file_loc):
               f = open(cfg_file_loc, 'r+')
            else:
               with open(cfg_file_loc, 'w+') as cfg_file:
                  cfg_file.write("hostname \"%s\"\n" % hostname)
                  cfg_file.write("tv_name \"%s\"\n" % tvname)
                  cfg_file.write("sv_password \"%s\"\n" % password)
                  cfg_file.write("exec comp.cfg")
               cfg_file.close()
            launchString = default_launch + " +map cp_badlands +exec \"" + cfg +  "\""
            launchString = "screen -S tf2comp" + str(i) + " " + launchString
	    print launchString
 	    os.system(launchString)
   else:
      launchString += "+map pl_badwater +maxplayers 24 +sv_pure 0"
      for i in range(1, int(args.count) +1):
         cfg = "tf2/cfg/%d.cfg" % ("pub" + str(i))
         with open(cfg, 'w+') as cfg_file:
            cfg_file.write("hostname \"%s\"" % hostname)
         string = launchString + " +exec pub" + str(i) + ".cfg"
         string = "screen -S tf2pub" + str(i) + " " + string
	 os.system(string)
main()
